#!/bin/bash -v
tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" puppet_pkg_url 
dpkg -i "$tempdeb"
apt-get update
apt-get -y install puppet-agent

echo "puppet_master_ip_address puppet" >> /etc/hosts

/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

/opt/puppetlabs/bin/puppet agent -t
/opt/puppetlabs/bin/puppet agent -t


cat <<EOF > /etc/network/interfaces.d/60-ens4.cfg
	auto ens4
	iface ens4 inet dhcp
EOF

reboot
systemctl restart networking.service
