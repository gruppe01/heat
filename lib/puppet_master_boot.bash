#!/bin/bash
tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" puppet_pkg_url
dpkg -i "$tempdeb"

apt-get update
apt-get -y install puppetserver golang ruby


/opt/puppetlabs/bin/puppet resource service puppetserver ensure=stopped enable=true
/opt/puppetlabs/bin/puppet resource service puppet ensure=stopped enable=true

manager_ip=$(ip a | grep -Eo 'inet ([0-9]*\.){3}[0-9]*' | tr -d 'inet ' | grep -v '^127')
echo $manager_ip
echo $manager_ip puppet >> /etc/hosts
/opt/puppetlabs/bin/puppet config set autosign true --section master

/opt/puppetlabs/bin/puppet module install puppet-r10k
cat <<EOF > /var/tmp/r10k.pp
class { 'r10k':
  sources => {
    'puppet' => {
      'remote'  => 'control_repo',
      'basedir' => '/etc/puppetlabs/code/environments',
      'prefix'  => false,
    },
  },
}
EOF
/opt/puppetlabs/bin/puppet apply /var/tmp/r10k.pp
r10k deploy environment -p

cat <<EOF >> /etc/crontab
*/10 * * * * root r10k deploy enviornment -p 
EOF
/opt/puppetlabs/bin/puppet resource service puppetserver ensure=running enable=true
/opt/puppetlabs/bin/puppet agent -t
/opt/puppetlabs/bin/puppet agent -t
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

gem install thor

mkdir /opt/go
export GOPATH=/opt/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

go get -u github.com/cloudflare/cfssl/cmd/cfssl
go get -u github.com/cloudflare/cfssl/cmd/cfssljson

chmod +x /etc/puppetlabs/code/environments/production/site/kubernetes/tooling/kube_tool.rb

reboot
